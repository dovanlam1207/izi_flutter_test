import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_getx_base/modules/campaign_detail/campaign_detail.dart';
import 'package:flutter_getx_base/shared/constants/colors.dart';
import 'package:flutter_getx_base/shared/constants/image_constant.dart';
import 'package:flutter_getx_base/shared/utils/size_utils.dart';
import 'package:flutter_getx_base/theme/theme_helper.dart';
import 'package:get/get.dart';
import 'package:readmore/readmore.dart';

import '../../shared/widgets/custom_text_style.dart';

class CamPaignDetailScreen extends GetView<CamPaignDetailController> {
  CamPaignDetailScreen({Key? key}) : super(key: key);

  final CamPaignDetailController camPaignDetailController =
      Get.put(CamPaignDetailController());

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      extendBody: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: ColorConstants.white,
      body: SingleChildScrollView(
        child: Obx(
          () => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(28),
                  child: camPaignDetailController.campaignsResponse.value !=
                          null
                      ? Image.network(
                          camPaignDetailController.listCampaigns[0].thumbnail,
                          height: MediaQuery.of(context).size.height * .5,
                          width: double.infinity,
                          fit: BoxFit.fitHeight,
                        )
                      : Image.asset(
                          ImageConstant.imgElderly,
                          height: MediaQuery.of(context).size.height * .5,
                          width: double.infinity,
                          fit: BoxFit.fitWidth,
                        ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(24),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Snacks for the elderly",
                          style: CustomTextStyles.labelBlack500Size24Fw700,
                        ),
                        SizedBox(
                          width: getSize(4),
                        ),
                        Text(
                          "${camPaignDetailController.campaignsResponse.value != null ? camPaignDetailController.dayCount(
                              camPaignDetailController
                                  .campaignsResponse.value!.startTime,
                              camPaignDetailController
                                  .campaignsResponse.value!.endTime,
                            ) : 20} day left",
                          style: CustomTextStyles.labelGray600Size14Fw400,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: getSize(20),
                    ),
                    Container(
                      width: double.infinity,
                      height: getSize(12),
                      decoration: BoxDecoration(
                        color: appTheme.gray300,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                            top: 0,
                            left: 0,
                            child: Container(
                              height: getSize(12),
                              width: camPaignDetailController
                                      .listCampaigns.isNotEmpty
                                  ? camPaignDetailController.percentTarget(
                                          camPaignDetailController
                                              .listCampaigns[0].amountTarget,
                                          camPaignDetailController
                                              .listCampaigns[0].currentAmount) *
                                      MediaQuery.of(context).size.width /
                                      100
                                  : MediaQuery.of(context).size.width * .4,
                              decoration: BoxDecoration(
                                color: appTheme.green300,
                                borderRadius: BorderRadius.circular(8),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: getSize(8),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Target = \$ ${camPaignDetailController.listCampaigns.isNotEmpty ? camPaignDetailController.listCampaigns[0].amountTarget : 25000}",
                          style: CustomTextStyles.labelGray500Size18Fw600,
                        ),
                        Text(
                          " ${camPaignDetailController.listCampaigns.isNotEmpty ? camPaignDetailController.percentTarget(camPaignDetailController.listCampaigns[0].amountTarget, camPaignDetailController.listCampaigns[0].currentAmount) : 48}%",
                          style: CustomTextStyles.labelGray500Size18Fw600,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: getSize(24),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            camPaignDetailController.campaignsResponse.value !=
                                    null
                                ? CircleAvatar(
                                    radius: 24,
                                    backgroundImage: CachedNetworkImageProvider(
                                      camPaignDetailController.campaignsResponse
                                              .value!.author.avatar ??
                                          "",
                                      maxWidth: getSize(20).toInt(),
                                    ),
                                  )
                                : CircleAvatar(
                                    backgroundImage: AssetImage(
                                      ImageConstant.imgElderly,
                                    ),
                                    radius: 24,
                                  ),
                            SizedBox(
                              width: getSize(16),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Sponsored by",
                                  style:
                                      CustomTextStyles.labelGray500Size16Fw600,
                                ),
                                Text(
                                  "${camPaignDetailController.listCampaigns.isNotEmpty ? camPaignDetailController.listCampaigns[0].author.name : "BenjaMin Evalent"}",
                                  style:
                                      CustomTextStyles.labelGrey500Size18Fw700,
                                ),
                              ],
                            ),
                          ],
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                            vertical: 6,
                            horizontal: 24,
                          ),
                          decoration: BoxDecoration(
                            color: appTheme.green300Bgr.withOpacity(.3),
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Text(
                            "Medical",
                            style: CustomTextStyles.labelGreen600Size16Fw400,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: getSize(20),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: ReadMoreText(
                        '${camPaignDetailController.campaignsResponse.value != null ? camPaignDetailController.campaignsResponse.value!.author.describe : "Flutter is Google’s mobile UI open source framework to build high-quality native (super fast) interfaces for iOS and Android apps with the unified codebase."} ',
                        trimLines: 3,
                        colorClickableText: Colors.pink,
                        trimMode: TrimMode.Line,
                        trimCollapsedText: 'see more',
                        trimExpandedText: 'see less',
                        moreStyle: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: appTheme.green300,
                        ),
                        lessStyle: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: appTheme.green300,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: getSize(24),
                    ),
                    Row(
                      children: [
                        camPaignDetailController.campaignsResponse.value !=
                                    null &&
                                camPaignDetailController
                                    .campaignsResponse.value!.backers.isNotEmpty
                            ? CircleAvatar(
                                radius: 24,
                                backgroundImage: CachedNetworkImageProvider(
                                  camPaignDetailController.campaignsResponse
                                      .value!.backers[0].avatar,
                                  maxWidth: getSize(20).toInt(),
                                ),
                              )
                            : CircleAvatar(
                                backgroundImage: AssetImage(
                                  ImageConstant.imgElderly,
                                ),
                                radius: 24,
                              ),
                        SizedBox(
                          width: getSize(24),
                        ),
                        Text(
                          "+${camPaignDetailController.campaignsResponse.value != null ? camPaignDetailController.campaignsResponse.value!.backers.length : 450} backers",
                          style: CustomTextStyles.labelGray500Size16Fw600,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: getSize(32),
                    ),
                    ElevatedButton(
                      onPressed: () {},
                      style: ElevatedButton.styleFrom(
                        primary: appTheme.green300,
                        onPrimary: Colors.white,
                        elevation: 3,
                        minimumSize: Size(double.infinity, 40),
                      ),
                      child: Text(
                        "BACK THIS PROJECTS",
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
