import 'package:dio/dio.dart';
import 'package:flutter_getx_base/models/response/campaigns/campaigns_response.dart';
import 'package:get/get.dart' as getx;
import 'package:get/get_rx/get_rx.dart';

class CamPaignDetailController extends getx.GetxController {
  Dio dio = Dio();

  RxList<CampaignsResponse> listCampaigns = <CampaignsResponse>[].obs;
  Rx<CampaignsResponse?> campaignsResponse = Rx<CampaignsResponse?>(null);

  @override
  void onInit() {
    fetchData();
    super.onInit();
  }

  void fetchData() async {
    try {
      Response response = await dio.get(
        'https://6551f0255c69a77903294d34.mockapi.io/v1/campaigns',
      );

      listCampaigns.assignAll(
        (response.data as List)
            .map((item) => CampaignsResponse.fromJson(item))
            .toList(),
      );
      campaignsResponse.value =
          listCampaigns.isNotEmpty ? listCampaigns[0] : null;
    } catch (error) {
      print('Error: $error');
    }
  }

  int dayCount(int startTimeStamp, int endTimeStamp) {
    DateTime startDate = DateTime.fromMillisecondsSinceEpoch(startTimeStamp);
    DateTime endDate = DateTime.fromMillisecondsSinceEpoch(endTimeStamp);

    int differenceInDays = endDate.difference(startDate).inDays;
    return differenceInDays;
  }

  double percentTarget(int value1, int value2) {
    if (value2 == 0) {
      return 0.0;
    }

    double percentage = (value1 / value2) * 100.0;
    return percentage;
  }
}
