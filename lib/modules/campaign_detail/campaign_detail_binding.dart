import 'package:flutter_getx_base/modules/campaign_detail/campaign_detail.dart';
import 'package:get/get.dart';

class CamPaignDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<CamPaignDetailController>(CamPaignDetailController());
  }
}
