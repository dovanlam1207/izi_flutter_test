class UsersResponse {
  final String? id;
  final String? email;
  final String? name;
  final String? avatar;
  final String? gender;
  final String? describe;
  final String? job;
  final String? password;

  UsersResponse({
    this.id,
    this.email,
    this.name,
    required this.avatar,
    this.gender,
    this.describe,
    this.job,
    this.password,
  });

  factory UsersResponse.fromJson(Map<String, dynamic> json) {
    return UsersResponse(
      id: json['id'],
      email: json['email'],
      name: json['name'],
      avatar: json['avatar'] ?? '',
      gender: json['gender']?.toString(),
      describe: json['describe']?.toString(),
      job: json['job']?.toString(),
      password: json['password']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'email': email,
      'name': name,
      'avatar': avatar,
      'gender': gender,
      'describe': describe,
      'job': job,
      'password': password,
    };
  }
}
